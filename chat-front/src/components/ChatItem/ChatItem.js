import React from 'react';
import {CardMedia, Paper} from "@material-ui/core";
import makeStyles from "@material-ui/core/styles/makeStyles";

const useStyles = makeStyles((theme) => ({
    itemMessage: {
        border: "2px solid grey",
        margin: "10px",
        padding: "5px",
    },
    cover: {
        width: '150px',
        height: '150px',
        overflow: 'hidden',
        borderRadius: '4px'
    },
}));

const ChatItem = (props) => {
    const classes = useStyles();

    return (
        <>
            <Paper className={classes.itemMessage}>
                <h4>Author: {props.author}</h4>
                <p>Message: {props.message}</p>
                <p>Date: {props.date}</p>
                <CardMedia
                    className={classes.cover}
                    image={'http://localhost:8000/uploads/' + props.image}
                />
            </Paper>

        </>
    );
};

export default ChatItem;