import React, {useEffect} from 'react';
import {CssBaseline, Toolbar} from "@material-ui/core";
import Container from "@material-ui/core/Container";
import {fetchMessages} from "../../store/actions/MessagesActions";
import {useDispatch, useSelector} from "react-redux";
import ChatItem from "../../components/ChatItem/ChatItem";


const GetMessages = () => {
    const dispatch = useDispatch();
    const messages = useSelector((state) => state.messages.messages);

    useEffect(() => {
        dispatch(fetchMessages())
    }, [dispatch]);

    return (
        <>
        <CssBaseline/>
            <Container>
                <Toolbar/>
                {messages.map((m, index) => (
                    <ChatItem
                        index={index}
                        key={m.id}
                        author={m.author}
                        message={m.message}
                        image={m.image}
                        date={m.date}
                    />
                ))}
            </Container>
        </>
    );
};

export default GetMessages;