import React, {useState} from 'react';
import Container from "@material-ui/core/Container";
import {CssBaseline, Grid, TextField, Toolbar} from "@material-ui/core";
import {useDispatch} from "react-redux";
import {createMessage} from "../../store/actions/MessagesActions";
import SendIcon from '@material-ui/icons/Send';
import FileInput from "../../components/UI/Form/FileInput";

const PostMessage = () => {
    const dispatch = useDispatch();

    const [data, setData] = useState({
        author: '',
        message: '',
        image: ''
    });

    const submitFormHandler = (e) => {
        e.preventDefault();
        const formData = new FormData();
        Object.keys(data).forEach(key => {
            formData.append(key, data[key]);
        });
        dispatch(createMessage(formData));
        setData({
            image: '',
            author: '',
            message: ''
        });
    };

    const onChangeHandler = (e) => {
        const name = e.target.name;
        const value = e.target.value;

        setData(prevState => ({
            ...prevState,
            [name]: value
        }));
    };

    const fileChangeHandler = e => {
        const name = e.target.name;
        const file = e.target.files[0];

        setData(prevState => ({
            ...prevState,
            [name]: file
        }));
    };

    return (
        <>
            <CssBaseline/>
            <Container>
                <Toolbar/>
                <form onSubmit={submitFormHandler}>
                <Grid container direction="column" spacing={2}>
                    <Grid item xs>
                        <TextField
                            fullWidth
                            required
                            name="message"
                            label="Message"
                            multiline
                            rows={2}
                            variant="outlined"
                            value={data.message}
                            onChange={onChangeHandler}
                        >
                        </TextField>
                    </Grid>
                    <Grid item xs>
                        <TextField
                            name="author"
                            label="Author"
                            variant="outlined"
                            value={data.author}
                            onChange={onChangeHandler}
                        />
                    </Grid>
                    <Grid item>
                        <FileInput
                            name="image"
                            label="Image"
                            value={data.image}
                            changeHandler={fileChangeHandler}
                            />
                    </Grid>
                    <Grid item>
                        <button onClick={submitFormHandler}><SendIcon/></button>
                    </Grid>
                </Grid>
                </form>
            </Container>
        </>
    );
};

export default PostMessage;