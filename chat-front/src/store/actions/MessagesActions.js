import axiosApi from "../../axiosApi";
import {NotificationManager} from 'react-notifications';

export const POST_MESSAGE = 'POST_MESSAGE';
export const FETCH_MESSAGES_REQUEST = 'FETCH_MESSAGES_REQUEST';
export const FETCH_MESSAGES_SUCCESS = 'FETCH_MESSAGES_SUCCESS';
export const FETCH_MESSAGES_FAILURE = 'FETCH_MESSAGES_FAILURE';

export const fetchMessagesRequest = () => ({type: FETCH_MESSAGES_REQUEST});
export const fetchMessagesSuccess = messages => ({type: FETCH_MESSAGES_SUCCESS, messages});
export const fetchMessagesFailure = () => ({type: FETCH_MESSAGES_FAILURE});

export const fetchMessages = () => {
    return async dispatch => {
        try {
            dispatch(fetchMessagesRequest());
            const response = await axiosApi.get('/messages');
            dispatch(fetchMessagesSuccess(response.data));
        } catch (e) {
            dispatch(fetchMessagesFailure());
            NotificationManager.error('Could not fetch products');
        }
    }
};

export const createMessage = (messageData) => {
    return async dispatch => {
        dispatch(fetchMessagesRequest());
        try{
            await axiosApi.post('/messages', messageData);
        } catch (e) {
            dispatch(fetchMessagesFailure(e));
        }
    }
};