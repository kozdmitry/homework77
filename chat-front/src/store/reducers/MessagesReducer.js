import {
    FETCH_MESSAGES_FAILURE,
    FETCH_MESSAGES_REQUEST,
    FETCH_MESSAGES_SUCCESS,
    POST_MESSAGE
} from "../actions/MessagesActions";

const initialState = {
    messages: [],
    messagesLoading: false,
    messagesError: false,
};

const messagesReducer = (state = initialState, action) => {
    switch (action.type) {
        case POST_MESSAGE:
            return {...state, message: action.input, author: action.input}
        case FETCH_MESSAGES_REQUEST:
            return {...state, messagesLoading: true};
        case FETCH_MESSAGES_SUCCESS:
            return {...state, messagesLoading: false, messages: action.messages};
        case FETCH_MESSAGES_FAILURE:
            return {...state, messagesError: false};
        default:
            return state;
    }
};

export default messagesReducer;