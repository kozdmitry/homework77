const express = require('express');
const fileDb = require('./fileDb');
const messages = require('./app/messages');
const cors = require("cors");

const app = express();
app.use(express.static('public'));
app.use(express.json());
app.use(cors());

app.use('/messages', messages);

const port = 8000;


const run = async () => {
    await fileDb.init();

    app.listen(port, () => {
        console.log(`Server started on ${port} port!`);
    });
}

run().catch(console.error)



