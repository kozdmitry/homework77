const express = require('express');
const path = require('path');
const fileDb = require('../fileDb');
const config = require('../config');
const router = express.Router();
const multer = require('multer');

const {nanoid} = require('nanoid');

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    },
});

const upload = multer({storage});


router.get('/', async (req, res) => {
    const messages = await fileDb.getItems();
    res.send(messages);
});

router.post('/', upload.single('image'), async (req, res) => {
    const message = req.body;

    if (message.author === '') {
        message.author = 'Anonymous'
    }
    if (req.file) {
        message.image = req.file.filename;
    }
    await fileDb.addItem(req.body);
    res.send(message);
});

module.exports = router;